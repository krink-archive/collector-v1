#!/usr/bin/env python

__version__ = '0.0.2.3'
debug = False

import subprocess
import multiprocessing
#import threading
import time
import os
import sys
import re


def sshCmd(host=None,cmd=None):
    cmdline = 'ssh -o LogLevel=Error -i .ssh/id_rsa  ' + host + ' ' + cmd
    #cmdline = 'ssh -o LogLevel=Error ' + host + ' ' + cmd

    sshcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, err = sshcmd.communicate()

    exit_code = sshcmd.wait()
    if (exit_code != 0):
        print 'Major Error:  not working properly ' + host
        return False

    if 'command not found' in err:
        print 'sysstat rpm not installed on ' + host
        return False

    now = time.strftime("%Y-%m-%d %H:%M:%S")
    epochtime = int(time.time())

    mlines = output.splitlines()

    odict = {}
    count = 0
    for line in mlines:
        count += 1
        odict[count] = line
        #print str(count) + ': ' + line
    #print str(odict)

    #print 'DIE DIE DIE'
    #return False

    mpstat_line = odict[4]
    #print 'mpstat_line is: ' + mpstat_line

    free_mem_line = odict[6]
    #print 'free_mem is: ' + free_mem_line

    free_swap_line = odict[8]
    #print 'free_swap is: ' + free_swap_line


    reObj1 = re.compile('^Filesystem')
    reObj2 = re.compile('^Device:')
    reObj3 = re.compile('^UID')

    #fs_line = device_line = uid_line = 0
    for key,value in odict.iteritems():
        if (reObj1.match(value)):
            #print key, odict[key]
            fs_line = key
        if (reObj2.match(value)):
            #print key, odict[key]
            device_line = key
        if (reObj3.match(value)):
            #print key, odict[key]
            uid_line = key

    #print 'fs_line : ' + str(fs_line)
    #print 'device_line : ' + str(device_line)
    #print 'uid_line : ' + str(uid_line)

    #fs_line_nums = odict.keys()[fs_line:device_line]
    #s = fs_line + 1
    e = device_line - 1
    fs_line_nums = odict.keys()[fs_line:e]
  
    root_fs_line = 'Empty' 
    for num in fs_line_nums:
        line = odict[num]
        #print  'fs_line: ' + line
        if line.endswith('/'):
            #print str(line)
            root_fs_line = line
            #print str(root_fs_line)

    #print 'root_fs_line ' + str(root_fs_line)
    #['/dev/vda1', '32G', '2.6G', '28G', '9%', '/']
    #['410G', '9.5G', '380G', '3%', '/']

    #device_line_nums = odict.keys()[device_line:uid_line]
    n = uid_line - 1
    device_line_nums = odict.keys()[device_line:n]

    disklist = []
    for num in device_line_nums:
        line = odict[num]

        #if re.match('^[a-zA-Z0-9_]+$',line):
        if re.match(r'^[a-zA-Z]', line):
            #print 'devline: ' + line 
            disklist.append(line)
    #print str(disklist)

    dirpath = 'collect/' + host + '/'
    if not os.path.isdir(dirpath):
        os.mkdir(dirpath, 0755)

    ##########################################################
    ##########################################################


    ##########################################################
    # mpstat

    #mpstat_vals = mpstat_line.split()
    #print str(mpstat_vals)

    mpstat_usr = mpstat_line.split()[3]
    #print 'mpstat_usr: ' + mpstat_usr
    mpstat_rrdupdate =  str(epochtime) + ':' + mpstat_usr
    #mpstat_rrdupdate =  'N:' + mpstat_usr

    mpstat_nice = mpstat_line.split()[4]
    #print 'mpstat_nice: ' + mpstat_nice
    mpstat_rrdupdate +=  ':' + mpstat_nice

    mpstat_sys = mpstat_line.split()[5]
    #print 'mpstat_sys: ' + mpstat_sys
    mpstat_rrdupdate +=  ':' + mpstat_sys

    mpstat_iowait = mpstat_line.split()[6]
    #print 'mpstat_iowait: ' + mpstat_iowait
    mpstat_rrdupdate +=  ':' + mpstat_iowait

    mpstat_irq = mpstat_line.split()[7]
    #print 'mpstat_irq: ' + mpstat_irq
    mpstat_rrdupdate +=  ':' + mpstat_irq

    mpstat_soft = mpstat_line.split()[8]
    #print 'mpstat_soft: ' + mpstat_soft
    mpstat_rrdupdate +=  ':' + mpstat_soft

    mpstat_steal = mpstat_line.split()[9]
    #print 'mpstat_steal: ' + mpstat_steal
    mpstat_rrdupdate +=  ':' + mpstat_steal

    mpstat_guest = mpstat_line.split()[10]
    #print 'mpstat_guest: ' + mpstat_guest
    mpstat_rrdupdate +=  ':' + mpstat_guest

    mpstat_idle = mpstat_line.split()[11]
    #print 'mpstat_idle: ' + mpstat_idle
    mpstat_rrdupdate +=  ':' + mpstat_idle

    #print 'mpstat_rrdupdate: ' + str(mpstat_rrdupdate)


    name = 'mpstat'
    txtupdate = str(mpstat_rrdupdate)
    rrdupdate = str(mpstat_rrdupdate)

    txtfile = dirpath + name + '.txt'
    rrdfile = dirpath + name + '.rrd'

    #print "rrdfile is " + rrdfile
    #print "txtfile is " + txtfile

    if not os.path.isfile(rrdfile):
        mpstatRRD(rrdfile)
    else:
        updateRRD(rrdfile, rrdupdate)
        updateTXT(txtfile, rrdupdate)

    ##########################################################
    # free mem

    #free_mem_vals = free_mem_line.split()
    #print str(free_mem_vals)

    mem_total = free_mem_line.split()[1]
    mem_used = free_mem_line.split()[2]
    mem_free = free_mem_line.split()[3]
    mem_shared = free_mem_line.split()[4]
    mem_buffers = free_mem_line.split()[5]
    mem_cached = free_mem_line.split()[6]

    freemem_rrdupdate =  str(epochtime) + ':' + mem_total
    freemem_rrdupdate += ':' + mem_used + ':' + mem_free
    freemem_rrdupdate += ':' + mem_shared + ':' + mem_buffers
    freemem_rrdupdate += ':' + mem_cached

    name = 'mem'
    txtfile = dirpath + name + '.txt'
    rrdfile = dirpath + name + '.rrd'

    txtupdate = str(freemem_rrdupdate)
    rrdupdate = str(freemem_rrdupdate)

    if not os.path.isfile(rrdfile):
        memRRD(rrdfile)
    else:
        updateRRD(rrdfile, rrdupdate)
        updateTXT(txtfile, rrdupdate)

    ##########################################################
    # free swap

    #freeswap_vals = free_swap_line.split()
    #print str(freeswap_vals)
    #5:              total       used       free     shared    buffers     cached
    #6: Mem:         48188      46186       2002          0         68      37471
    #7: -/+ buffers/cache:       8645      39543
    #8: Swap:        50383       2592      47791
    #['Swap:', '50383', '2592', '47791']

    swap_total = free_swap_line.split()[1]
    #print 'swap_total: ' + swap_total

    swap_used = free_swap_line.split()[2]
    #print 'swap_used: ' + swap_used

    swap_free = free_swap_line.split()[3]
    #print 'swap_free: ' + swap_free

    name = 'swap'
    txtfile = dirpath + name + '.txt'
    rrdfile = dirpath + name + '.rrd'

    freeswap_rrdupdate =  str(epochtime) + ':' + swap_total
    freeswap_rrdupdate += ':' + swap_used + ':' + swap_free

    txtupdate = str(freeswap_rrdupdate)
    rrdupdate = str(freeswap_rrdupdate)

    #print txtupdate

    if not os.path.isfile(rrdfile):
        swapRRD(rrdfile)
    else:
        updateRRD(rrdfile, rrdupdate)
        updateTXT(txtfile, rrdupdate)

    ##########################################################
    # df -h (root)
#9: Filesystem            Size  Used Avail Use% Mounted on
#10: /dev/mapper/vg_dbca403-lv_root
#11:                       410G  9.8G  379G   3% /
#12: tmpfs                  24G     0   24G   0% /dev/shm
#13: /dev/sda1             477M  182M  270M  41% /boot
#14: /dev/mapper/dbp1      4.0T  2.9T  1.2T  72% /db
#15: Linux 2.6.32-573.12.1.el6.x86_64 (db-ca4-02) 	07/28/2016 	_x86_64_	(32 CPU)
#16:
#17: Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn

#$ df -h
#Filesystem      Size  Used Avail Use% Mounted on
#/dev/md0         68G   55G  9.8G  85% /


    #root_fs_line
    root_fs_vals = root_fs_line.split()
    #print 'root_fs_line ' + str(root_fs_line)
    #['/dev/vda1', '32G', '2.6G', '28G', '9%', '/']
    #['410G', '9.5G', '380G', '3%', '/']

    if len(root_fs_vals) == 6:
        #print 'six items'
        offset = 1
    elif len(root_fs_vals) == 5:
        #print 'five items'
        offset = 0
    else:
        offset = 0

    #print 'DIE DIE DIE'
    #return False

    root_fs_size = root_fs_line.split()[0 + offset]
    #print 'root_fs_size: ' + root_fs_size

    root_fs_used = root_fs_line.split()[1 + offset]
    #print 'root_fs_used: ' + root_fs_used

    root_fs_avail = root_fs_line.split()[2 + offset]
    #print 'root_fs_avail: ' + root_fs_avail

    root_fs_use = root_fs_line.split()[3 + offset]
    #print 'root_fs_use: ' + root_fs_use
    #print 'root_fs_use trim: ' + root_fs_use[:-1]

    root_fs_name = root_fs_line.split()[4 + offset ]
    #print 'root_fs_name: ' + root_fs_name

    #print 'DIE DIE DIE'
    #return False

    name = 'root'
    txtfile = dirpath + name + '.txt'
    rrdfile = dirpath + name + '.rrd'

    root_rrdupdate =  str(epochtime) + ':' + root_fs_size
    root_rrdupdate += ':' + root_fs_used + ':' + root_fs_avail
    root_rrdupdate += ':' + root_fs_use[:-1]

    txtupdate = str(root_rrdupdate)
    rrdupdate = str(root_rrdupdate)

    #print txtupdate

    if not os.path.isfile(rrdfile):
        rootRRD(rrdfile)
    else:
        updateRRD(rrdfile, rrdupdate)
        updateTXT(txtfile, rrdupdate)


    ##########################################################
    # iostat -d
    #disklist

    #print 'disklist' + str(disklist)
    #16: Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
    #['vda', '0.34', '2.15', '3.65', '8926194', '15132728']

    disk_tps = disk_blkreads = disk_blkwrtns = disk_blkrd = disk_blkwr = 0
    for item in disklist:
        #item.split()
        #print str(item.split())
        this_disk_name = item.split()[0]
        this_disk_tps  = item.split()[1]
        this_disk_blkreads  = item.split()[2]
        this_disk_blkwrtns  = item.split()[3]
        this_disk_blkrd  = item.split()[4]
        this_disk_blkwr  = item.split()[5]

        #print 'Type is disk_tps ' + str(type(disk_tps)) + str(disk_tps)
        #print 'Type is this_disk_tps ' + str(type(this_disk_tps)) + str(float(this_disk_tps))

        disk_tps = disk_tps + float(this_disk_tps)
        disk_blkreads = disk_blkreads + float(this_disk_blkreads)
        disk_blkwrtns = disk_blkwrtns + float(this_disk_blkwrtns)
        disk_blkrd = disk_blkrd + float(this_disk_blkrd)
        disk_blkwr = disk_blkwr + float(this_disk_blkwr)
        #name = disk_name

        #print 'Type disk_blkwr ' + str(type(disk_blkwr)) + ' ' + str(disk_blkwr)
        #if disk_blkwr == '0':
        #    print 'skip ' + str(name)
        #    continue

    name = 'iostat'

    if debug: print 'collecting ' + name

    disk_rrdupdate =  str(epochtime) + ':' + str(disk_tps)
    disk_rrdupdate += ':' + str(disk_blkreads) + ':' + str(disk_blkwrtns)
    disk_rrdupdate += ':' + str(disk_blkrd) + ':' + str(disk_blkwr)

    txtfile = dirpath + name + '.txt'
    rrdfile = dirpath + name + '.rrd'

    txtupdate = str(disk_rrdupdate)
    rrdupdate = str(disk_rrdupdate)

    if not os.path.isfile(rrdfile):
        diskRRD(rrdfile)
    else:
        updateRRD(rrdfile, rrdupdate)
        updateTXT(txtfile, rrdupdate)

    ##########################################################
    # ps -ef

    #print 'uid_line : ' + str(uid_line)

    s = uid_line
    e = odict.keys()[-1]
    #print 'last key : ' + str(e)

    ps_line_nums = odict.keys()[s:e]

    number_of_procs = e - s

    number_of_defunct = 0
    for num in ps_line_nums:
        line = odict[num]

        if re.search(r'<defunct>', line):
            #print 'psline: ' + line
            number_of_defunct += 1


    ps_rrdupdate = str(epochtime) + ':' + str(number_of_procs)
    ps_rrdupdate += ':' + str(number_of_defunct)

    name = 'ps'
    txtfile = dirpath + name + '.txt'
    rrdfile = dirpath + name + '.rrd'
    txtupdate = str(ps_rrdupdate)
    rrdupdate = str(ps_rrdupdate)

    if not os.path.isfile(rrdfile):
        psRRD(rrdfile)
    else:
        updateRRD(rrdfile, rrdupdate)
        updateTXT(txtfile, rrdupdate)
        
    if debug: print 'Done ' + host
    print 'Done ' + host
    return True




#def updateRRD(name='Empty',rrdfile=None, rrdupdate=None):
def updateRRD(rrdfile=None, rrdupdate=None):
    if debug: print('rrdtool update ' + rrdfile + ' ' + rrdupdate)
    os.system('rrdtool update ' + rrdfile + ' ' + rrdupdate)
    return True

def updateTXT(txtfile=None, rrdupdate=None):
    if debug: print 'append file ' + txtfile + ' ' + rrdupdate
    with open(txtfile, "a") as outfile:
        outfile.write(rrdupdate + '\n')
    return True

def psRRD(rrdfile=None):
#ps_rrdupdate = str(epochtime) + ':' + number_of_procs
#ps_rrdupdate += ':' + number_of_defunct
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:procs:GAUGE:600:U:U '
    cmdline += ' DS:defunct:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    print "cmdline: " + cmdline
    os.system(cmdline)
    return True    

def diskRRD(rrdfile=None):
#['dm-4', '342.73', '1670.65', '2661.46', '24692779871', '39337324729']
#Device:            tps   Blk_read/s   Blk_wrtn/s   Blk_read   Blk_wrtn
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:tps:GAUGE:600:U:U '
    cmdline += ' DS:blk_reads:GAUGE:600:U:U '
    cmdline += ' DS:blk_wrtns:GAUGE:600:U:U '
    cmdline += ' DS:blk_read:GAUGE:600:U:U '
    cmdline += ' DS:blk_wrtn:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    print "cmdline: " + cmdline
    os.system(cmdline)
    return True



def rootRRD(rrdfile=None):
#root_fs_size: 32125
#root_fs_used: 2566
#root_fs_avail: 27922
#root_fs_use: 9%
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:size:GAUGE:600:U:U '
    cmdline += ' DS:used:GAUGE:600:U:U '
    cmdline += ' DS:avail:GAUGE:600:U:U '
    cmdline += ' DS:use:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    print "cmdline: " + cmdline
    os.system(cmdline)
    return True    


def mpstatRRD(rrdfile=None):
# mpstat
# 03:27:26 PM  CPU    %usr   %nice    %sys %iowait    %irq   %soft  %steal  %guest   %idle
# 03:27:26 PM  all    1.48    0.00    0.59    0.25    0.00    0.02    0.00    0.00   97.66
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:usr:GAUGE:600:U:U '
    cmdline += ' DS:nice:GAUGE:600:U:U '
    cmdline += ' DS:sys:GAUGE:600:U:U '
    cmdline += ' DS:iowait:GAUGE:600:U:U '
    cmdline += ' DS:irq:GAUGE:600:U:U '
    cmdline += ' DS:soft:GAUGE:600:U:U '
    cmdline += ' DS:steal:GAUGE:600:U:U '
    cmdline += ' DS:guest:GAUGE:600:U:U '
    cmdline += ' DS:idle:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    print "cmdline: " + cmdline
    os.system(cmdline)
    return True

# the round robin archive
#'RRA:AVERAGE:0.5:1:360'    Archive point is saved every 5min, archive is kept for 1day 6hour back.
#'RRA:AVERAGE:0.5:12:1008'  Archive point is saved every 1hour, archive is kept for 1month 11day back.
#'RRA:AVERAGE:0.5:288:2016' Archive point is saved every 1day, archive is kept for 5year 6month 8day back.

def memRRD(rrdfile=None):
# free -m
#             total       used       free     shared    buffers     cached
#Mem:          3819       3553        266          0        251       2977
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:total:GAUGE:600:U:U '
    cmdline += ' DS:used:GAUGE:600:U:U '
    cmdline += ' DS:free:GAUGE:600:U:U '
    cmdline += ' DS:shared:GAUGE:600:U:U '
    cmdline += ' DS:buffers:GAUGE:600:U:U '
    cmdline += ' DS:cached:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    print "cmdline: " + cmdline
    os.system(cmdline)
    return True

def swapRRD(rrdfile=None):
# free -m
#             total       used       free
#Swap:         5999          0       5999
    #print "running rrdtool create"
    cmdline = 'rrdtool create ' + rrdfile
    cmdline += ' --start 0 --step 300 '
    cmdline += ' DS:total:GAUGE:600:U:U '
    cmdline += ' DS:used:GAUGE:600:U:U '
    cmdline += ' DS:free:GAUGE:600:U:U '
    cmdline += ' RRA:AVERAGE:0.5:1:360 '
    cmdline += ' RRA:AVERAGE:0.5:12:1008 '
    cmdline += ' RRA:AVERAGE:0.5:288:2016 '
    print "cmdline: " + cmdline
    os.system(cmdline)
    return True


#cmd = 'mpstat ; free -m; df -h; iostat -d; ps -ef'
if __name__ == "__main__":

  if sys.argv[1:]:
    infile = sys.argv[1]
  else:
    infile = 'hosts.txt'
 
  #print 'infile ' + str(infile)
  try:
    #with open('hosts.txt.two', 'r') as file:
    with open(infile, 'r') as file:
        #hosts = file.readlines() #has '\n'
        hosts = file.read().splitlines()
  except IOError as e:
    print 'Unable to open file ' + str(infile)
    sys.exit(1)

  if debug: print 'hosts are: ' + str(hosts)
  #print 'DIE DIE DIE'
  #sys.exit(1)

  processes = []
  for host in hosts:
    cmd = 'mpstat;free -m;df -m;iostat -d;ps -ef'
    p = multiprocessing.Process(target=sshCmd, args=(host,cmd,))
    p.start()
    processes.append(p)
    if debug: print 'Launched process ' + str(p) + ' ' + str(host)

  for p in processes: p.join()


sys.exit(0)


