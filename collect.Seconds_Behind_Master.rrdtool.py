#!/usr/bin/env python

import subprocess
import threading
import time
import os

def sshCmd(host=None,cmd=None):
    cmdline = 'ssh -o LogLevel=Error -i .ssh/id_rsa  ' + host + ' ' + cmd
    subcmd = subprocess.Popen(cmdline.split(), stdout=subprocess.PIPE)
    output = subcmd.communicate()
    exit_code = subcmd.wait()
    if (exit_code != 0):
        print 'Major Error:  not working properly ' + host
    now = time.strftime("%Y-%m-%d %H:%M:%S")
    epochtime = int(time.time())

    #print now + ' ' + host + ' ' + str(output)
    val = output[0].strip()

    txtupdate = now + ' ' + host + ' ' + val
    rrdupdate = str(epochtime) + ':' + val

    dirpath = 'collect/' + host + '/'
    if not os.path.isdir(dirpath):
        os.mkdir(dirpath, 0755)

    name = 'sbm'

    #txtfile = dirpath + host + '.' + name + '.txt'
    #rrdfile = dirpath + host + '.' + name + '.rrd'

    txtfile = dirpath + name + '.txt'
    rrdfile = dirpath + name + '.rrd'

    with open(txtfile, "a") as outfile:
        outfile.write(txtupdate + '\n')

    #print "rrdfile is " + rrdfile

    if not os.path.isfile(rrdfile):
        #print "running rrdtool create"
        cmdline = 'rrdtool create ' + rrdfile
        cmdline += ' --start 0 --step 300 '
        cmdline += ' DS:%s:GAUGE:600:0:U ' % name
        cmdline += ' RRA:AVERAGE:0.5:1:360 '
        cmdline += ' RRA:AVERAGE:0.5:10:1008 '
        #print "cmdline: " + cmdline
        os.system(cmdline)

    os.system('rrdtool update ' + rrdfile + ' ' + rrdupdate)

hosts = ['db-ca4-01',
         'db-ca4-02',
         'db-ca4-03',
         'db-ca4-04',
         'db-ca4-05',
         'db-ca4-06',
         'db-ca4-07',
         'db-ca4-08',
         'db-ca4-09',
         'db-ca4-10',
         'db-ca4-11',
         'db-ca4-12',
         'db-ca4-13',
         'db-ca4-14',
         'db-ma1-01',
         'db-ma1-02',
         'db-ma1-03',
         'db-ma1-04',
         'db-ma1-05',
         'db-ma1-06',
         'db-ma1-07',
         'db-ma1-08',
         'db-ma1-09',
]

threads = []
for host in hosts:
    #sshCmd(host,cmd='uptime')
    #cmd = 'uptime'
    cmd = "mysql -u XXXX -pXXXX -e 'show slave status \G;' | grep Seconds_Behind_Master: | awk -F: '{print $2}'"
    t = threading.Thread(target=sshCmd, args=(host,cmd,))
    t.start()
    threads.append(t)

for t in threads: t.join()


